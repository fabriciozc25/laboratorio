<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * Database settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/documentation/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Database settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'lab2' );

/** Database username */
define( 'DB_USER', 'root' );

/** Database password */
define( 'DB_PASSWORD', '' );

/** Database hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'E0TSzYA9TVr>aOTM~xRR.YL$Pu=pqHT]6ug{,3E5H>6Yrn(>>>O%tl|9.?Gd5>kS' );
define( 'SECURE_AUTH_KEY',  '>Lqg`nAiD|n{Y>SBt!MTEvkL_J#[CK[7|()C(n<QQ~eNlzKs:2qO> /Q;x=jjZEU' );
define( 'LOGGED_IN_KEY',    '^79T`Zls>dUG{~*8-=Qf0njWL:JN#-C_v8;P$1)(bz|~,6t~.NBwXmwH/%N9W*Mx' );
define( 'NONCE_KEY',        'Qf=7_1$,![G:Oo@75E<ZX/%>plyx _x%7W[D7_!P7(.2gd3y0T&o`DvN3{vs^x)0' );
define( 'AUTH_SALT',        'A6[rV^ba#rH.P?!+ZY[InJ>&gf(FH&qhC$?b@AuJDo? 6e$%0RL]v3a(]l v <ZH' );
define( 'SECURE_AUTH_SALT', 'E>95OC<_}.Zg%9$wWFRFP23bNK;{9Pgd:j4JID14{xo0 t{9vMv?wG/=foP]&zr9' );
define( 'LOGGED_IN_SALT',   'XlK]Eco.#C{eqkKOgm(+X$bT/hl2VxFhG#JB@G9&FT}d%T7sRSDExJUiUrwNHIE:' );
define( 'NONCE_SALT',       'ziS(U!N-cnz$WC[.*Y.y(Zl+O.+WG+5#>{Z,R`Gf`[#thb,oJgrR4uy,#rzn:X(6' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/documentation/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
